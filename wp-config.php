<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NAF3wOnXMNDdfXWciDEFl8zq9zThXevgigapM4adYPD6/sskom+kQQOFUIN2KtluntHDma7qt6X3T4y7QUD2EQ==');
define('SECURE_AUTH_KEY',  'f0FzFYOZ0LLHzSwhbf4UGeKPwC6qgfhUpR4zyijFoEm9hL7onVSJGdRUuUUtW0yBTUTl9bFko3agmSPR7tSMDQ==');
define('LOGGED_IN_KEY',    'xUd3RQsHON3qtN83s6aONjxvkmBD/5HeWg/G/clo/R2QoTZaAGXeIT1IxM/ThtusQ0RErJu9eMvFPcjLQz9aEQ==');
define('NONCE_KEY',        'CX3S1d4yN8mplsmJxMwiJVNj4W177Iut+k13DO5r/muc4De53DnhaFFul4uGHcUSwJ5k3eFvmO9j974bJRcjZw==');
define('AUTH_SALT',        'uS2TmzWR9cnwZ3PiKvDehw9iU6NbhAyPBUOGesmWhOzJasQOwiKuNAA+yIjxM30Fnfhn2/V1rMUP6h8yj75ZMQ==');
define('SECURE_AUTH_SALT', 'x4pbAXpnCDIE6Su0pTrYiFDqOWDTu2K6Y9uLLQy0852WpkXllc2qPAGPhElGsksNRJIy2h7VegQBpfh7EuF/RA==');
define('LOGGED_IN_SALT',   'aP6OchQlFEAUhnLVmllTvjpjoHtAThlBbKev/v1HO9I6Tw6kxAA0edNDfn1zcB4bRr992kcyYuhTu2fS01QZKw==');
define('NONCE_SALT',       '9FPtQAEW8JMR0gHojw8TaB0Io1Uda+0rNjGeZbeB51XH/ajhDZfMUvfXMVKQV6dzKvLv/rmu47GRDnTNRmy+aA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
